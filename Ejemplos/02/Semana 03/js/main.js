class Alumno {
  constructor(nombre, edad, semestre) {
    this.nombre = nombre;
    this.edad = edad;
    this.semestre = semestre;
  }
}
class Egresado extends Alumno {}
function holaLet(mundo) {
  let hola = "hola";
  console.info(hola);
  mundo();
}
function mundoLet() {
  hola = "mundo";
  console.info(hola);
}
function print() {
  if (true) {
    let test = 0;
    console.log(test);
  }
  if (true) {
    test = 5;
    console.log(test);
  }
}

function AJAX() {
  let alum = new Alumno("Gabriel", 19, "Tercero");
  console.log(alum);
  let request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.status === 200 && this.readyState === 4) {
      console.log(this.responseText);
      // let data = JSON.stringify(this.responseText);
      let data = JSON.parse(this.responseText);
      console.log(data);
      console.log(data._comment);
      // console.log(data["_comment"]);
      data.alumnos.forEach(alumno => {
        console.log(alumno);
        let container = document.getElementById("alumnos");
        // container.prepend(`<ol>
        // <li>Nombre: ${alumno.nombre}</li>
        // <li>Edad: ${alumno.edad}</li>
        // <li>Semestre: ${alumno.semestre}</li>
        // </ol>
        // `);
        container.innerHTML += `<ul>
            <li>Nombre: ${alumno.nombre}</li>
            <li>Edad: ${alumno.edad}</li>
            <li>Semestre: ${alumno.semestre}</li>
            </ul>
            `;
      });
    }
  };
  request.open("GET", "../mock/data.json", true);
  request.send();
}

function main() {
  // holaLet(mundoLet);
  //   print();
  console.log("READY");
  let form = document.getElementById("signin");

  form.addEventListener("submit", function(e) {
    let alert = document.getElementById("alert");
    alert.innerHTML = "";
    try {
      console.log("SUBMIT");
      console.log(`Nombre: ${form["name"].value}`);
      let pass = document.getElementById("password");
      if (pass.value.length === 0) {
        pass.classList.add("fail");
        let a = 2;
        let b = "2";
        throw "Contraseña vacia";
        // throw a * b;
      } else {
        pass.classList.remove("fail");
      }
    } catch (error) {
      alert.innerHTML = error;
    }
    e.preventDefault();
  });
}
window.onload = main;
